//Escena: Disposiscion de elementos para mostrar en pantalla
//1.. Crear un objeto escena
let escena = new THREE.Scene();

//2.. Crear un Objeto camara
let resolucion = window.innerWidth/window.innerHeight;
let camara =new THREE.PerspectiveCamera(75,resolucion ,1 , 100);
camara.position.z = 60;

//3..Crear un objeto que nos ayude a "dibujar" la escena en la pantalla "(RENDERIZAR)"
let lienzo = new THREE.webGLRender();
lienzo.setSize(window.innerWidth,window.innerHeight);
//etiqueta canvas
document.body.appendChild(lienzo.domElement);

//4..Crearlos objetos que pertenecen a la escena
//Geometria:Un objeto que representa un un elemento geometrico
let geometriabase = new THREE.TorusKnotGeometry( 10, 3, 100, 16 )
//Material:Recubrimiento de una geometria
let material = new THREE.MeshBasicMaterial({color:0x000FF});
///Malla(mash): Modelo de una Geometria
let miCubo = new THREE.Mesh(geometriabase, material);
escena.add(miCubo);
//crear animacion
let animar = function(){

    requestAnimationFrame(animar)
    //miCubo.rotation.x=miCubo.rotation.x+0.5
    //miCubo.rotation.y=micUBO.rotation.y+0.5
    for(miCubo.position.x=-15;miCubo.position.x<16;){
        miCubo.position.x=miCubo.position.x+0.05
    }
    for(miCubo.position.x=15;miCubo.position.x>-16;){
        miCubo.position.x=miCubo.position.x-0.05
    }
    
    //miCubo.position.x=miCubo.position.x+0.5
    //miCubo.position.y=miCubo.position.y+0.5

    lienzo.render(escena, camara)
}

animar()




